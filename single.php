 <?php while (have_posts()) : the_post(); ?>

 	<?php 
 	if (has_post_thumbnail( $post->ID ) ) {
 		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
 		$src=$image[0]; 
 	}
 	else {
 		$src=get_template_directory_uri().'/dist/images/noimage.png'; 
 	}
 	?>

 	<section class="blogEntry">
 		<div class="blog__image">
 			<div class="blog__image-bg"></div><img src="<?php echo $src;?>" alt="">
 		</div>
 		<div class="row">
 			<div class="blogEntry__title">
 				<div class="title center"><span><?php the_title();?></span></div>
 				<div class="blogEntry-date"><span><?php the_date('d.m.Y');?></span></div>
 			</div>
 			<div class="column">
 				<div class="content-block large-8 large-offset-2">
 					<?php the_content();?>
 				</div>
 			</div>
 		</div>
 	</section>

 <?php endwhile; ?>



 <?php $args = array( 
 	'posts_per_page' 	=> 2,
 	'offset'			=> 0, 
 	'exclude'      		=> $post->ID
 	);
 	$posts = get_posts( $args ); ?>
 	<?php if (count($posts)>=2) : ?>

 		<section class="other-articles-wrap">
 			<div class="row">
 				<div class="other-articles clearfix">
 					<div class="other-articles__title">
 						<div class="title"><span><?php _e('Інші статті','lionline');?></span></div>
 					</div>
 					<?php
 					foreach ( $posts as $post ) : setup_postdata( $post ); ?>
 					<?php 
 					if (has_post_thumbnail( $post->ID ) ) {
 						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
 						$src=$image[0]; 
 					}
 					else {
 						$src=get_template_directory_uri().'/dist/images/noimage.png'; 
 					}
 					?>
 					<article>
 						<div class="article-item column large-5">
 							<div class="article-item__img">
 								<a href="<?php echo get_permalink(); ?>">
 									<img src="<?= $src;?>" alt="">
 								</a>
 							</div>
 							<div class="article-item__date"><span><?php the_date('d.m.Y');?></span></div>
 							<div class="article-item__title">
 								<a href="<?php echo get_permalink(); ?>">
 									<span><?php the_title(); ?></span>
 								</a>
 							</div>
 							<div class="article-item__text">
 								<p><?php echo get_the_excerpt();?></p>
 							</div>
 							<div class="article-item__btn"><a class="btn" href="<?php echo get_permalink(); ?>"><?php _e('Читати','lionline'); ?></a></div>
 						</div>
 					</article>

 				<?php endforeach; 
 				wp_reset_postdata();?>
 			</div>
 		</div>
 	</section>
 <?php endif ?>
