<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<?php get_template_part('templates/head'); ?>
<body>
	<?php get_template_part('templates/header'); ?>

	<?php include Wrapper\template_path(); ?>

	<?php wp_footer();?>
	<?php get_template_part( 'templates/form', 'booking' );?>


	<?php get_template_part( 'templates/footer','reviews' );?>
	<script>
		
		// jQuery(document).ready(function(){ 
		// 	var oldpos = 0;
		// 	var mywindow = jQuery(window);

		// 	if (jQuery(window).width() < 640) {

		// 		jQuery('header').addClass('fixed');
		// 		mywindow.scroll(function() {
		// 			var newpos=window.pageYOffset;

		// 			if ((oldpos<newpos) && (newpos>50) ) {
		// 				jQuery('header').removeClass('fixed');  
		// 			}
		// 			else {
		// 				jQuery('header').addClass('fixed');
		// 			}
		// 			oldpos = newpos;
		// 		});

		// 	} else {
		// 		jQuery("header").removeClass('fixed');
		// 		jQuery(window).scroll(function() {
		// 			if (jQuery(this).scrollTop() > 46){  
		// 				jQuery("header").addClass('fixed');
		// 			}
		// 			else  {
		// 				jQuery("header").removeClass('fixed');
		// 			}

		// 		});

		// 	}

		// });


	</script>
</body>
</html>