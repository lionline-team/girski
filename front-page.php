<?php while (have_posts()) : the_post(); ?>

  <slider>
    <div class="homeSlider" id="homeSlider">

      <?php if( have_rows('intro_slider') ):?>
        <?php while ( have_rows('intro_slider') ) : ?>
          <?php the_row(); ?>

          <div class="homeSlider-item">
            <div class="homeSlider-item__img"><img src="<?php the_sub_field('image');?>" alt=""></div>
            <div class="row">
              <div class="homeSlider-item__info">
                <div class="slider_title"><span><?php the_sub_field('title');?></span></div>
                <div class="slider_text">
                  <p><?php the_sub_field('text');?></p>
                </div>
                <div class="slider_btn">
                  <?php $button=get_sub_field('button');?>
                  <?php if ($button) : ?>
                    <a class="btn " href="<?= $button['url'];?>"><?= $button['title'];?></a>
                  <?php else : ?>
                    <?php $button=get_field('phone',pll_current_language('slug'));  ?>
                    <a class="btn " data-open="openform"><?php _e('Забронювати','lionline');?></a>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        <?php  endwhile; ?>
      <?php endif; ?>
    </div>
  </slider>
  <section class="about-wrap">
    <div class="row">
      <div class="about-hotel clearfix" id="about">
        <div class="about-hotel__img column large-7">
          <img src="<?php the_field('image1');?>" alt="">
          <img src="<?php the_field('image3');?>" alt="">

          <img src="<?php the_field('image2');?>" alt="">

        </div>
        <div class="about-hotel__content column large-5">
          <div class="title"><span><?php the_field('about_title');?></span></div>
          <div class="about-hotel__text">
            <p><?php the_field('about_text');?></p>
          </div>
          <?php $button=get_field('about_button');?>
          <?php if ($button) : ?>
            <div class="about-hotel__btn"><a class="btn btn_f1" href="<?= $button['url'];?>"><?= $button['title'];?></a>
            <?php else: ?>
              <?php $button=get_field('phone',pll_current_language('slug'));  ?>
              <div class="about-hotel__btn"><a class="btn btn_f1" data-open="openform"><?php _e('Забронювати','lionline');?></a>
              <?php endif;?>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>


  <?php get_template_part( 'templates/block','rooms_front' );?>

  <?php get_template_part( 'templates/block','restaurant' );?>

  <?php get_template_part( 'templates/block','spa' );?>

  <?php get_template_part( 'templates/block','conveniences' );?>

  <?php get_template_part( 'templates/block','advantages' );?>

  <?php get_template_part( 'templates/block','reviews' );?>


<?php endwhile; ?>
