<?php while (have_posts()) : the_post(); ?>
	<section>
		<div class="blog__image">
			<div class="blog__image-bg"></div><img src="<?php echo get_template_directory_uri();?>/dist/images/foto.png" alt="">
		</div>
	</section>
	<section>
		<div class="row">
			<div class="video clearfix">
				<div class="video__title">
					<div class="title center"><span><?php the_title();?></span></div>
				</div>
				<div class="column clearfix">
					<div class="content-block large-8 large-offset-2">
						<?php the_content();?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endwhile; ?>
