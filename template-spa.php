<?php
/**
 * Template Name: SPA Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
 <section class="roomSlider-wrap">
  <slider>
    <div class="roomSlider">
     <?php
     $images = get_field('gallery');
     $size = 'full';

     if( $images ): ?>
     <?php foreach( $images as $image ): ?>
       <article>
        <div class="roomSlider-item">
          <div class="roomSlider-item__img">
            <div class="slide-bg"></div><?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
          </div>
        </div>
      </article>
    <?php endforeach; ?>
  <?php endif; ?>
</div>
<div class="roomSlider__title"><span class="btn"><?php the_title();?></span></div>
</slider>
</section>
<section class="SPA-wrap">
  <div class="row">
    <div class="restaurant-menu">
      <div class="restaurant-menu__title">
        <div class="title"><span>SPA</span></div>
      </div>
      <div class="restaurant-menu__slider column large-6">
        <div class="menuSlider">
         <?php if(get_field('spa_gallery')): ?>
          <?php while(has_sub_field('spa_gallery')): ?>
            <article>
              <div class="menuSlider-item">
                <div class="menuSlider-item__img"><img src="<?php the_sub_field('image'); ?>" alt=""></div>
                <div class="menuSlider-item__text"><span><?php the_sub_field('text'); ?></span></div>
              </div>
            </article>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
    <div class="restaurant-menu__info column large-6">
      <div class="menu-title"><span><?php the_field('spa_title');?></span></div>
      <div class="restaurant-menu-text">
        <p><?php the_field('spa_text');?></p>
      </div>
      <?php $button=get_field('spa_gallery',pll_current_language('slug'));  ?>
      <?php if ($button) : ?>
        <div class="menu-btn"><a class="btn" href="<?= $button['url'];?>"><span><?= $button['title'];?></span></a></div>
      <?php else : ?>
        <?php $button=get_field('phone',pll_current_language('slug'));  ?>
        <div class="menu-btn"><a class="btn"  data-open="openform"><?php _e('Забронювати','lionline');?></a></div>
      <?php endif; ?>
    </div>
  </div>
</div>
</section>
<section>
  <div class="row">
    <div class="vine-card clearfix">
      <div class="vine-card__title">
        <div class="title right"><span><?php _e('басейн','lionline');?></span></div>
      </div>
      <div class="vine-card__info column large-5">
        <div class="vine-card-title"><span><?php the_field('pool_title');?></span></div>
        <div class="vine-card-text">
          <p><?php the_field('pool_text');?></p>
        </div>
        <?php $button=get_field('pool_link',pll_current_language('slug'));  ?>
        <?php if ($button) : ?>
          <div class="vine-card-btn"><a class="btn" href="<?= $button['url'];?>"><span><?= $button['title'];?></span></a></div>
        <?php else : ?>
          <?php $button=get_field('phone',pll_current_language('slug'));  ?>
          <div class="vine-card-btn"><a class="btn"  data-open="openform"><?php _e('Забронювати','lionline');?></a></div>
        <?php endif; ?>
      </div>
      <div class="vine-card__foto column large-6"><img src="<?php the_field('pool_image1');?>" alt=""><img src="<?php the_field('pool_image2');?>" alt=""></div>
    </div>
  </div>
</section>
<section class="spa-sauna">
  <div class="row">
    <div class="vine-card vine-card_spa clearfix">
      <div class="vine-card__title">
        <div class="title"><span><?php _e('сауна','lionline');?></span></div>
      </div>
      <div class="vine-card__foto column large-6"><img src="<?php the_field('bath_image1');?>" alt=""><img src="<?php the_field('bath_image2');?>" alt=""></div>
      <div class="vine-card__info column large-5">
        <div class="vine-card-title"><span><?php the_field('bath_title');?></span></div>
        <div class="vine-card-text">
          <p><?php the_field('bath_text');?></p>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="row">
    <div class="vine-card clearfix">
      <div class="vine-card__title">
        <div class="title right"><span><?php _e('Хамам','lionline');?></span></div>
      </div>
      <div class="vine-card__info column large-5">
        <div class="vine-card-title"><span><?php the_field('hammam_title');?></span></div>
        <div class="vine-card-text">
          <p><?php the_field('hammam_text');?></p>
        </div>
        <?php $button=get_field('hammam_link',pll_current_language('slug'));  ?>
        <?php if ($button) : ?>
          <div class="vine-card-btn"><a class="btn" href="<?= $button['url'];?>"> <span><?= $button['title'];?></span></a></div>
        <?php else : ?>
          <?php $button=get_field('phone',pll_current_language('slug'));  ?>
          <div class="vine-card-btn"><a class="btn"  data-open="openform"><?php _e('Забронювати','lionline');?></a></div>
        <?php endif; ?>

      </div>
      <div class="vine-card__foto column large-6"><img src="<?php the_field('hammam_image1');?>" alt=""><img src="<?php the_field('hammam_image2');?>" alt=""></div>
    </div>
  </div>
</section>
<script>
 jQuery(document).ready(function(){
  jQuery(".roomSlider").slick({
    slidesToShow: 1,
    arrows:true,
    autoplaySpeed: 6000,
    speed:1000,
    autoplay:true,
    dots:true,
    prevArrow: '<img class="room-arrow slide-prev" src="<?php echo get_template_directory_uri();?>/dist/images/slide-prev.svg" alt="">',
    nextArrow: '<img class="room-arrow slide-next" src="<?php echo get_template_directory_uri();?>/dist/images/slide-next.svg" alt="">',
    // fade:true,
    dotsClass: 'slick-control',
    responsive: [

    {
      breakpoint: 640,
      settings: {
       arrows:false,
       slidesToShow: 1
     }
   }
   ]

 });


  jQuery(".menuSlider").slick({
    slidesToShow: 1,
    arrows:true,
    autoplaySpeed: 6000,
    speed:1000,
    autoplay:true,
    dots:false,
    prevArrow: '<img class="room-arrow slide-prev" src="<?php echo get_template_directory_uri();?>/dist/images/slide-prev.svg" alt="">',
    nextArrow: '<img class="room-arrow slide-next" src="<?php echo get_template_directory_uri();?>/dist/images/slide-next.svg" alt="">',
    // fade:true,
    // dotsClass: 'slick-control',
    responsive: [

      {
        breakpoint: 640,
        settings: {
         arrows:false,

       }
     }
   ]
  });




});

</script>

<?php get_template_part( 'templates/block','rooms' );?>


<?php endwhile; ?>
