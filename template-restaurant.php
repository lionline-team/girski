<?php
/**
 * Template Name: Restaurant Template
 */
?>
<?php while (have_posts()) : the_post(); ?>

 <section class="restaurantSlider">
  <slider>
    <div class="roomSlider">
      <<<<<<< HEAD
      <?php
      $images = get_field('gallery');
      $size = 'full';

      if( $images ): ?>
      <?php foreach( $images as $image ): ?>
       <article>
        <div class="roomSlider-item">
          <div class="roomSlider-item__img">
            <div class="slide-bg"></div><?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
          </div>
        </div>
      </article>
    <?php endforeach; ?>
  <?php endif; ?>
</div>
<div class="restaurantSlider__title"><span class="btn"><?php the_title();?></span></div>
</slider>
</section>
<section class="restaurant-menu-wrap">
  <div class="row">
    <div class="restaurant-menu">
      <div class="restaurant-menu__title">
        <div class="title"><span><?php _e('меню','lionline');?></span></div>
      </div>
      <div class="restaurant-menu__slider column large-6">
        <div class="menuSlider">

          <?php if(get_field('menu_gallery')): ?>
            <?php while(has_sub_field('menu_gallery')): ?>

              <article>
                <div class="menuSlider-item">
                  <div class="menuSlider-item__img"><img src="<?php the_sub_field('image'); ?>" alt=""></div>
                  <div class="menuSlider-item__text"><span><?php the_sub_field('text'); ?></span></div>
                </div>
              </article>

            <?php endwhile; ?>
          <?php endif; ?>

        </div>
      </div>
      <div class="restaurant-menu__info column large-6">
        <div class="menu-title"><span><?php the_field('menu_title');?></span></div>
        <div class="restaurant-menu-text">
          <p><?php the_field('menu_text');?></p>
        </div>
        <?php $button=get_field('menu_link',pll_current_language('slug'));  ?>
        <?php if ($button) : ?>
          <div class="menu-btn"><a class="btn btn_brown" href="<?= $button['url'];?>"> <span><?= $button['title'];?></span></a></div>
        <?php else : ?>
          <?php $button=get_field('phone',pll_current_language('slug'));  ?>
          <div class="menu-btn"><a class="btn btn_brown"  data-open="openform"> <span><?php _e('Забронювати','lionline');?></span></a></div>

        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="row">
    <div class="vine-card clearfix">
      <div class="vine-card__title">
        <div class="title right"><span><?php _e('Винна карта','lionline');?></span></div>
      </div>
      <div class="vine-card__info column large-5">
        <div class="vine-card-title"><span><?php the_field('vine_title');?></span></div>
        <div class="vine-card-text">
          <p><?php the_field('vine_text');?></p>
        </div>

        <?php $button=get_field('vine_link',pll_current_language('slug'));  ?>
        <?php if ($button) : ?>
          <div class="vine-card-btn"><a class="btn" href="<?= $button['url'];?>"><?= $button['title'];?></a></div>
        <?php else : ?>
          <?php $button=get_field('phone',pll_current_language('slug'));  ?>
          <div class="vine-card-btn"><a class="btn"  data-open="openform"> <?php _e('Забронювати','lionline');?></a></div>
        <?php endif; ?>
      </div>
      <div class="vine-card__foto column large-6"><img src="<?php the_field('vine_image1');?>" alt=""><img src="<?php the_field('vine_image2');?>" alt=""></div>
    </div>
  </div>
</section>
<section>
  <div class="row">
    <div class="breakfasts clearfix">
      <div class="breakfasts__title">
        <div class="title"><span><?php _e('Сніданок','lionline');?></span></div>
      </div>
      <div class="breakfasts__img column large-6"><img src="<?php the_field('breakfast_image');?>" alt=""></div>
      <div class="breakfasts__info column large-6">
        <div class="breakfasts-title"><span><?php the_field('breakfast_title');?></span></div>
        <div class="breakfasts-text">
          <p><?php the_field('breakfast_text');?></p>
        </div>
        <?php $button=get_field('breakfast_link',pll_current_language('slug'));  ?>
        <?php if ($button) : ?>
          <div class="breakfasts-btn"><a class="btn" href="<?= $button['url'];?>"> <span><?= $button['title'];?></span></a></div>
        <?php else: ?>
          <?php $button=get_field('phone',pll_current_language('slug'));  ?>
          <div class="breakfasts-btn"><a class="btn"  data-open="openform"><?php _e('Забронювати','lionline');?></a></div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
<script>
 jQuery(document).ready(function(){
  jQuery(".roomSlider").slick({
    slidesToShow: 1,
    arrows:true,
    autoplaySpeed: 6000,
    speed:1000,
    autoplay:true,
    dots:true,
    prevArrow: '<img class="room-arrow slide-prev" src="<?php echo get_template_directory_uri();?>/dist/images/slide-prev.svg" alt="">',
    nextArrow: '<img class="room-arrow slide-next" src="<?php echo get_template_directory_uri();?>/dist/images/slide-next.svg" alt="">',
    // fade:true,
    dotsClass: 'slick-control',
    responsive: [

    {
      breakpoint: 640,
      settings: {
       arrows:false,
       slidesToShow: 1
     }
   }
   ]
 });



  jQuery(".menuSlider").slick({
    slidesToShow: 1,
    arrows:true,
    autoplaySpeed: 6000,
    speed:1000,
    autoplay:true,
    dots:false,
    prevArrow: '<img class="room-arrow slide-prev" src="<?php echo get_template_directory_uri();?>/dist/images/slide-prev.svg" alt="">',
    nextArrow: '<img class="room-arrow slide-next" src="<?php echo get_template_directory_uri();?>/dist/images/slide-next.svg" alt="">',
    // fade:true,
    // dotsClass: 'slick-control',
     responsive: [

      {
        breakpoint: 640,
        settings: {
         arrows:false,

       }
     }
   ]
  });
});
</script>

<?php endwhile; ?>

<?php get_template_part( 'templates/block','rooms' );?>

<?php get_template_part( 'templates/block','advantages' );?>
