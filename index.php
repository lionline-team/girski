<?php
/**
 * Template Name: Restaurant Template
 */
?>
<section>
  <div class="blog__image">
    <img src="<?php echo get_template_directory_uri();?>/dist/images/blog_bg.png" alt="">
    <div class="blog__image__title"><span>Блог</span></div>
  </div>
  <div class="last-news">
    <div class="row">
      <div class="blog__title">
        <div class="title"><span><?php _e('Новини','lionline');?></span></div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="blog-items">

      <?php while (have_posts()) : the_post(); ?>
        <?php 
        if (has_post_thumbnail( $post->ID ) ) {
          $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
          $src=$image[0]; 
        }
        else {
          $src=get_template_directory_uri().'/dist/images/noimage.png'; 
        }
        ?>

        <article>
          <div class="blog-item clearfix">
            <div class="blog-item__img column large-5">
              <a href="<?php echo get_permalink(); ?>">
                <img src="<?= $src;?>" alt="">
              </a>
            </div>
            <div class="blog-item__content column large-7">
              <div class="blog-item-date"><span><?php the_date('d.m.Y');?></span></div>
              <div class="blog-item-title">
                <a href="<?php echo get_permalink(); ?>">
                  <span><?php the_title(); ?></span>
                </a>
              </div>
              <div class="blog-item-text">
                <p><?php echo get_the_excerpt();?></p>
              </div>
              <div class="blog-item-btn"><a class="btn" href="<?php echo get_permalink(); ?>"><?php _e('Читати','lionline'); ?></a></div>
            </div>
          </div>
        </article>

      <?php endwhile; ?>

      <div class="nav-links">
        <a class="prev page-numbers" href="#"></a>
        <span class="page-numbers current" aria-current="page">1</span>
        <a class="page-numbers" href="#">2</a><a class="page-numbers" href="#">3</a>
        <a class="next page-numbers" href="#"></a>
      </div>
    </div>
  </div>
</section>

<?php get_template_part( 'templates/block','rooms' );?>

<?php get_template_part( 'templates/block','advantages' );?>