<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<?php wp_head(); ?>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Lora:400,700|Montserrat:500,400|Roboto:300,400,500,700" rel="stylesheet">
  <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri();?>/dist/images/favicon.png">

  <!-- start TL head script -->
  <script type="text/javascript">
    (function(w){
      var q=[
      ['setContext', 'TL-INT-girski', '<?php echo pll_current_language('slug'); ?>']
      ];
      var t=w.travelline=(w.travelline||{}),ti=t.integration=(t.integration||{});ti.__cq=ti.__cq?ti.__cq.concat(q):q;
      if (!ti.__loader){ti.__loader=true;var d=w.document,p=d.location.protocol,s=d.createElement('script');s.type='text/javascript';s.async=true;s.src=(p=='https:'?p:'http:')+'//eu-ibe.tlintegration.com/integration/loader.js';(d.getElementsByTagName('head')[0]||d.getElementById('getElementbyId('bookingwrapper')')[0]).appendChild(s);}
    })(window);
  </script>
  <!-- end TL head script -->


  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130566939-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-130566939-1');
  </script>


  <!-- Yandex.Metrika counter -->
  <script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
     m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js", "ym")

   ym(51452687, "init", {
    id:51452687,
    clickmap:true,
    trackLinks:true,
    accurateTrackBounce:true,
    webvisor:true
  });
</script>
<!-- /Yandex.Metrika counter -->

</head>
