<section>
  <div class="row">
    <div class="conveniences" id="conv">
      <div class="conveniences__title">
        <div class="title center"><span><?php the_field('amenties_title',pll_current_language('slug'));  ?></span></div>
      </div>
      <div class="conveniences__items">
       <?php if( have_rows('amenties',pll_current_language('slug')) ):?>
        <?php while ( have_rows('amenties',pll_current_language('slug')) ) : ?>
          <?php the_row(); ?>
          <div class="conveniences-item">
            <div class="conveniences-item__img"><img src="<?php the_sub_field('photo');?>" alt=""></div>
            <div class="conveniences-item__title"><span><?php the_sub_field('title');?></span></div>
            <div class="conveniences-item__text">
              <p><?php the_sub_field('text');?></p>
            </div>
          </div>
        <?php  endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</div>
</section>