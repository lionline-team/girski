<section>
	<div class="homeform" id="cont">
		<div class="row">
			<div class="homeform__title">
				<div class="title center"><span><?php the_field('prefooter_title',pll_current_language('slug'));  ?></span></div>
			</div>
			<div class="homeform__text">
				<?php the_field('prefooter_text',pll_current_language('slug'));  ?>
			</div>
			<?php $button=get_field('phone',pll_current_language('slug'));  ?>
			<div class="homeform__btn"><a class="btn btn_f1 btn_brown"  data-open="openform"><span><?php _e('Забронювати','lionline');?></span></a></div>
			<div class="homeform__phone"><a class="btn_f2" href="<?= $button['url'];?>">
				<img src="<?php echo get_template_directory_uri();?>/dist/images/phone.svg" alt=""><img src="<?php echo get_template_directory_uri();?>/dist/images/phone-b.svg" alt=""></a>
			</div>
		</div>
	</div>
</section>
<footer>
	<div class="footer clearfix">
		<div class="row">
			<div class="footer__contacts large-5 medium-5 small-12 columns">
				<div class="footer-addres">
					<p><?php the_field('footer_string1',pll_current_language('slug')); ?></p>
				</div>
				<div class="footer-contacts">
					<div class="footer-soc">
						<a href="<?php the_field('facebook_link',pll_current_language('slug'));  ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/fb.svg" alt=""></a>
						<a href="<?php the_field('instagram_link',pll_current_language('slug'));  ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/inst.svg" alt=""></a></div>
						<div class="footer-phone">
							<ul>
								<?php $button=get_field('phone',pll_current_language('slug'));  ?>
								<?php if ($button ) : ?>
									<li><a href="<?= $button['url'];?>"><?= $button['title'];?></a></li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer__logo large-2 medium-3 small-12 columns"><a href="#"><img src="<?php echo get_template_directory_uri();?>/dist/images/footer-logo.png" alt=""></a></div>
				<div class="footer__copy large-5 medium-4 small-12 columns">
					<div class="lionline">
						<span><?php the_field('footer_string2',pll_current_language('slug'));  ?></span>
					</div>
					<div class="copy"> Created by <a rel="noreferrer" href="https://lionline.agency" target="_blank">LIONLINE </a>©</div>
				</div>
			</div>
		</div>
	</footer>
