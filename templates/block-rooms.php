<section class="rooms-wrap">
  <div class="row">
    <div class="title right"><span><?php the_field('rooms_title',pll_current_language('slug'));  ?></span></div>
    <div class="rooms-hotel clearfix" id="rooms">
      <div class="rooms-hotel__content column large-5 medium-12">
        <div class="rooms-hotel-title"><span><?php _e('Номери для пари або компанії','lionline');?></span></div>
        <div class="rooms-hotel__text">
          <p><?php the_field('rooms_text',pll_current_language('slug'));  ?></p>
        </div>

        <?php if ($button) : ?>
          <div class="rooms-hotel__btn"><a class="btn btn_brown" href="<?= $button['url'];?>"><span><?= $button['title'];?></span></a></div>
        <?php else : ?>
          <?php $button=get_field('phone',pll_current_language('slug'));  ?>
          <div class="rooms-hotel__btn"><a class="btn btn_brown"  data-open="openform"><span><?php _e('Забронювати','lionline');?></span></a></div>
        <?php endif; ?>

      </div>
      <div class="rooms-hotel__rooms column large-7 medium-12">
        <div class="room-items">
          <?php if( have_rows('rooms',pll_current_language('slug')) ):?>
            <?php while ( have_rows('rooms',pll_current_language('slug')) ) : ?>
              <?php the_row(); ?>
              <a class="room-item btn_f1" href="<?php the_sub_field('link');?>">
                <div class="room-item__class-room"><span><?php the_sub_field('title');?></span><p><?php the_sub_field('subtitle');?></p></div>

                <div class="room-item__img"><img src="<?php the_sub_field('image');?>" alt=""></div>
                <div class="room-item__price"><span><?php the_sub_field('price');?></span></div>
                <div class="room-item__info"><span><?php the_sub_field('text');?></span></div>
              </a>
            <?php  endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>

</section>
