<section class="restaurant-wrap">
  <div class="row">
    <div class="restaurant">
      <div class="restaurant__title">
        <div class="title centerFW"><span><?php the_field('rest_title',pll_current_language('slug'));  ?></span></div>
      </div>
      <div class="restaurant__info column large-6">
        <div class="restaurant-title"><span><?php the_field('rest_subtitle',pll_current_language('slug'));  ?></span></div>
        <div class="restaurant-text">
          <p><?php the_field('rest_text',pll_current_language('slug'));  ?></p>
        </div>

        <?php $button=get_field('rest_button',pll_current_language('slug'));  ?>
        <?php if ($button) : ?>
          <div class="restaurant-btn"><a class="btn" href="<?= $button['url'];?>"><?= $button['title'];?></a></div>
        <?php else : ?>
          <?php $button=get_field('phone',pll_current_language('slug'));  ?>
          <div class="restaurant-btn"><a class="btn"  data-open="openform"><?php _e('Забронювати','lionline');?></a></div>
        <?php endif; ?>

      </div>
      <div class="restaurant__img column large-6"><img src="<?php the_field('rest_image',pll_current_language('slug'));  ?>" alt=""></div>

    </div>
  </section>
