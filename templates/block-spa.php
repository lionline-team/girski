ƒ<section class="spa_wrap">
<div class="row">
  <div class="spa clearfix">
    <div class="spa__img column large-6">
      <div class="spa-img">
        <img src="<?php the_field('spa_image1',pll_current_language('slug'));  ?>" alt="">
        <img src="<?php the_field('spa_image2',pll_current_language('slug'));  ?>" alt="">
        <img src="<?php the_field('spa_image3',pll_current_language('slug'));  ?>" alt="">
      </div>
    </div>
    <div class="spa__content column large-4">
      <div class="title"><span><?php the_field('spa_title',pll_current_language('slug'));  ?></span></div>
      <div class="spa__text">
        <p><?php the_field('spa_text',pll_current_language('slug'));  ?></p>
      </div>

      <?php $button=get_field('spa_link',pll_current_language('slug'));  ?>
      <?php if ($button) : ?>
        <div class="spa__btn"><a class="btn btn_f1" href="<?= $button['url'];?>"><?= $button['title'];?></a></div>
      <?php else: ?>
        <?php $button=get_field('phone',pll_current_language('slug'));  ?>
        <div class="spa__btn"><a class="btn btn_f1"  data-open="openform"><?php _e('Забронювати','lionline');?></a></div>
      <?php endif; ?>
    </div>
  </div>
</div>
</section>
