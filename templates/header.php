<header class="fixed">
  <div class="header header_transparent">
    <div class="header-top-wrap">
      <div class="row">
        <div class="header__top">
          <div class="top-menu">
            <nav>
              <ul>
                <?php if (has_nav_menu('header1_navigation')) :?>
                  <?php wp_nav_menu(['theme_location' => 'header1_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
                <?php endif;?>
              </ul>
            </nav>
          </div>
          <?php $button=get_field('phone',pll_current_language('slug'));  ?>
          <?php if ($button ) : ?>
            <div class="top-contacts"><span><?php _e('Бронювання за телефоном:','lionline');?></span><a href="<?= $button['url'];?>"><?= $button['title'];?></a></div>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="header__menu">
      <div class="mob_open">
        <div class="icon">
          <div class="burger"></div>
        </div>
      </div>
      <div class="logo-mob"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo-white" src="<?php echo get_template_directory_uri();?>/dist/images/footer-logo.png" alt=""></a></div>
      <div class="row">
        <div class="nav-menu">
          <ul class="nav-menu_left h-menu">
            <?php if (has_nav_menu('header2_navigation')) :?>
              <?php wp_nav_menu(['theme_location' => 'header2_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
            <?php endif;?>
          </ul>
          <div class="header__logo h-menu"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo-black" src="<?php echo get_template_directory_uri();?>/dist/images/form_logo.png" alt=""><img class="logo-white" src="<?php echo get_template_directory_uri();?>/dist/images/footer-logo.png" alt=""></a></div>
          <ul class="nav-menu_right">
            <?php if (has_nav_menu('header3_navigation')) :?>
              <?php wp_nav_menu(['theme_location' => 'header3_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
            <?php endif;?>
          </ul>
          <div class="bottom-mob-menu show-for-small-only">
            <nav>
              <ul>
                <?php if (has_nav_menu('header1_navigation')) :?>
                  <?php wp_nav_menu(['theme_location' => 'header1_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
                <?php endif;?>
              </ul>
            </nav>
            <div class="bottom-mob-menu__phone">
              <?php $button=get_field('phone',pll_current_language('slug'));  ?>
              <?php if ($button ) : ?>
                <p><?php _e('Бронювання за телефоном:','lionline');?></p><a href="<?= $button['url'];?>"><?= $button['title'];?></a>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
