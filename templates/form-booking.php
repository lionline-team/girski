<div class="reveal" id="openform" data-reveal="">
  <div class="popupForm clearfix">
    <div class="form__logo"><img src="<?php echo get_template_directory_uri();?>/dist/images/form_logo.png" alt=""></div>
    <section id='bookingwrapper'>
      <!-- start TL Booking form script -->

      <div id="tl-booking-form">&nbsp;</div>
      <script type="text/javascript">
        (function(w){
          var q=[
          ['setContext', 'TL-INT-girski', '<?php echo pll_current_language('slug'); ?>'],
          ['embed', 'booking-form', {container: 'tl-booking-form'}]
          ];
          var t=w.travelline=(w.travelline||{}),ti=t.integration=(t.integration||{});ti.__cq=ti.__cq?ti.__cq.concat(q):q;
          if (!ti.__loader){ti.__loader=true;var d=w.document,p=d.location.protocol,s=d.createElement('script');s.type='text/javascript';s.async=true;s.src=(p=='https:'?p:'http:')+'//eu-ibe.tlintegration.com/integration/loader.js';(d.getElementsByTagName('head')[0]||d.getElementbyId('bookingwrapper')[0]).appendChild(s);}
        })(window);
      </script>
    </section>
  </div>
</div>

    <!-- end TL Booking form script -->
    <!--  <div class="popupForm clearfix">
      <div class="form__logo"><img src="<?php echo get_template_directory_uri();?>/dist/images/form_logo.png" alt=""></div>
      <div class="form_tabs">
        <ul>s
          <li class="form1 tab-active" data-collection="1" data-triger="0"><?php _e('БРОНЮВАННЯ','lionline');?></li>
          <li class="form2" data-collection="1" data-triger="1"><?php _e('ЗВ’ЯЗАТИСЬ','lionline');?></li>
        </ul>
      </div>
      <div class="booking_form pop_form active" data-collection="1" data-target="0">
        <div class="contact_us__success">
          <h2><?php _e('ВІДПРАВЛЕННЯ ЗАЯВКИ УСПІШНЕ!','lionline');?></h2>
          <p><?php _e('Ви успішно відправили заявку до готелю “Гірскі”. Очікуйте дзвінка від менеджера.','lionline');?></p>
          <button class="close-button" id="btn_success" data-close="" aria-label="Close reveal" type="button"><?php _e('Закрити','lionline');?></button>
        </div>
        <form class="book_form">
          <div class="large-6 medium-6 small-12">
            <p><?php _e('Категорія номеру','lionline');?></p>
            <select class="selectroom" name="classRoom">
              <option><?php _e('Стандарт','lionline');?></option>
              <option><?php _e('Люкс','lionline');?></option>
            </select>
          </div>
          <div class="large-6 medium-6 small-12">
            <div class="number">
              <div class="Adults large-6 medium-6">
                <p><?php _e('Дорослі:','lionline');?></p>
                <div class="value-button" id="decrease" value="Decrease Value">-</div>
                <input class="adultscount" type="number" name="Adults" id="number" value="2">
                <div class="value-button" id="increase" value="Increase Value">+</div>
              </div>
              <div class="Children large-6 medium-6">
                <p><?php _e('Діти:','lionline');?></p>
                <div class="value-button" id="decrease2" value="Decrease Value">-</div>
                <input class="childscount" type="number" name="Children" id="number2" value="0">
                <div class="value-button" id="increase2" value="Increase Value">+</div>
              </div>
            </div>
          </div>
          <div class="large-6 medium-6 small-12">
            <p><?php _e('Заїзд','lionline');?></p>
            <input class="datepicker-begin" readonly type="text" name="date_begin" id="date_begin" required>
          </div>
          <div class="large-6 medium-6 small-12">
            <p><?php _e('Виїзд','lionline');?></p>
            <input class="datepicker-finish" readonly type="text" id="date_end" name="date_end" required>
          </div>
          <div class="large-6 medium-6 small-12">
            <p><?php _e('Ім’я','lionline');?></p>
            <input class="contactname" type="text" name="name" placeholder="">
          </div>
          <div class="large-6 medium-6 small-12">
            <p><?php _e('Телефон','lionline');?></p>
            <input class="contactphone" type="text" name="phone" required placeholder="">
          </div>
          <div class="large-12 medium-12 small-12">
            <p><?php _e('Коментарі','lionline');?></p>
            <textarea class="contacttext" placeholder="<?php _e('Вкажіть додаткові побажання, чи уточнення','lionline');?>"></textarea>
          </div>
          <div class="form_text">
            <p><?php _e('Передоплата в розмірі 50% від суми, заїзд — до 14:00, виїзд — після 12:00','lionline');?></p>
          </div>
          <div class="popupForm__btn columns">
            <button class="button success" type="submit"><?php _e('Забронювати','lionline');?></button>
          </div>
        </form>
      </div>
      <div class="contact_form clearfix pop_form" data-collection="1" data-target="1">
        <div class="contact_us__success_contact-form">
          <h2>ВІДПРАВЛЕННЯ ЗАЯВКИ УСПІШНЕ!</h2>
          <p>Ви успішно відправили заявку до готелю “Гірскі”. Очікуйте дзвінка від менеджера.</p>
          <button class="close-button" id="btn_success_contact-form" data-close="" aria-label="Close reveal" type="button">Закрити</button>
        </div>
        <div class="form-text">
          <p><?php _e('Зателефонуйте нам, або заповніть форму і ми вам передзвоннм','lionline');?></p>
          <?php $button=get_field('phone',pll_current_language('slug'));  ?>
          <?php if ($button ) : ?>
            <a href="<?= $button['url'];?>"><?= $button['title'];?></a>
          <?php endif; ?>
        </div>
        <div class="columns">
          <form class="contact-form">
            <p><?php _e('Ім’я','lionline');?>
            <input class="contactname" type="text">
          </p>
          <p><?php _e('Телефон','lionline');?>
          <input class="contactphone" required type="text">
        </p>
        <div class="popupForm__btn columns">
          <button class="button success" type="submit"><?php _e('Відправити','lionline');?></button>
        </div>
      </form>
    </div>
  </div>
  <div class="form__img"><img src="<?php echo get_template_directory_uri();?>/dist/images/form-img.png" alt=""></div>
</div>
<button class="close-button" data-close="" aria-label="Close reveal" type="button"><span aria-hidden="true">×</span></button> -->
<!-- </div> -->


<!-- form action -->
<!-- <script>
  if ( typeof book_form_hook_defined === 'undefined') { // No dublicate hooks, when use two forms on one page

  jQuery(document).on('submit','form.book_form',function(e){


  jQuery(this).find('button').addClass('animate');
  e.preventDefault();
  setTimeout(function(){
  // jQuery(".book_form").hide();
  jQuery('.button').removeClass('animate');
  jQuery(".contact_us__success").addClass('active');
},1000);


jQuery("#btn_success").click(function(){
jQuery(".contact_us__success").removeClass('active');
})


var room=jQuery(this).find('select.selectroom :selected').text();

var adults = jQuery(this).find('input.adultscount').val();
var childs = jQuery(this).find('input.childscount').val();
var indate = jQuery(this).find('input.datepicker-begin').val();
var outdate = jQuery(this).find('input.datepicker-finish').val();
var name = jQuery(this).find('input.contactname').val();
var phone = jQuery(this).find('input.contactphone').val();

var text = jQuery(this).find('textarea.contacttext').val();

jQuery.ajax({
url: ajaxurl,
data: {
'action':'book',
'adults':adults,
'childs':childs,
'room':room,
'indate':indate,
'outdate':outdate,
'name':name,
'phone':phone,
'text':text,
},
success:function(data) {
console.log('sended!');
console.log(data);
jQuery(this).trigger("reset");

},
error: function(errorThrown){
console.log(errorThrown);
}
});
e.preventDefault(e);
});
book_form_hook_defined=true;
}
</script>
-->
<!-- <script>
  if ( typeof contact_form_hook_defined === 'undefined') { // No dublicate hooks, when use two forms on one page

  jQuery(document).on('submit','form.contact-form',function(e){

  jQuery(this).find('button').addClass('animate');
  e.preventDefault();
  jQuery(this).trigger("reset");
  setTimeout(function(){
  // jQuery(".book_form").hide();
  jQuery('.button').removeClass('animate');
  jQuery(".contact_us__success_contact-form").addClass('active');
},1000);


jQuery("#btn_success_contact-form").click(function(){
jQuery(".contact_us__success_contact-form").removeClass('active');
})



var name = jQuery(this).find('input.contactname').val();
var phone = jQuery(this).find('input.contactphone').val();

jQuery.ajax({
url: ajaxurl,
data: {
'action':'call',
'name':name,
'phone':phone,
},
success:function(data) {
console.log('sended!');
console.log(data);

},
error: function(errorThrown){
console.log(errorThrown);
}
});
e.preventDefault(e);
});
contact_form_hook_defined=true;

}
</script> -->
