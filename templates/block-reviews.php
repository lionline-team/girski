<?php if( have_rows('reviews',pll_current_language('slug')) ):?>

  <section class="reviews-wrap">
    <div class="reviews__title">
      <div class="title center"><span><?php the_field('reviews_title',pll_current_language('slug'));  ?></span></div>
    </div>
    <div class="reviews clearfix" id="rev">
      <div class="row">
        <div class="reviews-slider">

          <?php while ( have_rows('reviews',pll_current_language('slug')) ) : ?>
            <?php the_row(); ?>

            <article class="column large-4 medium-6">
              <div class="reviews-item">
                <div class="reviews-item__img"><img src="<?php the_sub_field('photo');?>" alt=""></div>
                <div class="reviews-item__content">
                  <div class="reviews-item__name"><span><?php the_sub_field('name');?></span></div>
                  <div class="reviews-item__date"><span><?php the_sub_field('title');?></span></div>
                  <div class="reviews-item__text"><span><?php the_sub_field('text');?></span></div>
                </div>
              </div>
            </article>

          <?php  endwhile; ?>

        </div>
        <?php $button=get_field('reviews_button',pll_current_language('slug'));  ?>
        <?php if ($button) : ?>
          <div class="reviews__btn"><a class="btn" href="<?= $button['url'];?>"><?= $button['title'];?></a></div>
        <?php else : ?>
          <?php $button=get_field('phone',pll_current_language('slug'));  ?>
          <div class="reviews__btn"><a class="btn" href="<?= $button['url'];?>"  data-open="openform"><?php _e('Забронювати','lionline');?></a></div>
        <?php endif; ?>

      </div>
    </div>

  </section>
<?php endif; ?>
<script>
  jQuery(document).ready(function(){
    jQuery(".reviews-slider").slick({
      slidesToShow: 2,
      arrows:true,
      autoplaySpeed: 6000,
      speed:1000,
      autoplay:false,
      prevArrow: '<img class="room-arrow slide-prev" src="<?php echo get_template_directory_uri();?>/dist/images/slide-prev.svg" alt="">',
      nextArrow: '<img class="room-arrow slide-next" src="<?php echo get_template_directory_uri();?>/dist/images/slide-next.svg" alt="">',
      responsive: [
      {
        breakpoint: 1024,
        settings: {

          slidesToShow: 2
        }
      },
      {
        breakpoint: 640,
        settings: {

          slidesToShow: 1
        }
      }
      ]

    });
  });

</script>
