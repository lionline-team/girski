<section>
  <div class="advanteges-wrap" >
    <div class="row">
      <div class="advanteges clearfix">
        <div class="advanteges__content column large-5">
          <div class="advanteges__cont">
            <div class="advanteges-title"><div class="title"><span><?php the_field('adv_title',pll_current_language('slug'));  ?></span></div></div>
            <div class="advanteges-text">
              <p><?php the_field('adv_subtitle',pll_current_language('slug'));  ?></p>
            </div>
            <div class="advanteges-icons">
              <?php if( have_rows('adv_advantages',pll_current_language('slug')) ):?>
                <?php while ( have_rows('adv_advantages',pll_current_language('slug')) ) : ?>
                  <?php the_row(); ?>
                  <div class="advanteges-icon large-4 small-6 column">
                    <div class="advanteges-icon__img"><img src="<?php the_sub_field('thumb');?>" alt=""></div>
                    <div class="advanteges-icon__text"><span><?php the_sub_field('text');?></span></div>
                  </div>
                <?php  endwhile; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div class="advanteges__img column large-7"><img src="<?php the_field('adv_image',pll_current_language('slug'));  ?>" alt=""></div>
      </div>
    </div>
  </div>
</section>