 <section>
 	<div class="blog__image">
 		<div class="blog__image-bg"></div><img src="<?php echo get_template_directory_uri();?>/dist/images/foto.png" alt="">
 	</div>
 </section>
 <section>
 	<div class="row">
 		<div class="video clearfix">
 			<div class="video__title">
 				<div class="title center"><span><?php _e('Сторінку не знайдено','lionline');?></span></div>
 			</div>
 			<div class="column clearfix">
 				<div class="content-block large-8 large-offset-2">
 					<div class="_404">
 						<div class="_404__title"><span><?php _e('Помилка 404','lionline');?></span></div>
 						<div class="_404__text">
 							<p><?php _e('Сторінку яку ви шукаєте видалено або не переміщено. ','lionline');?></p>
 							<p><?php _e('Ви можете зв’язатись з нашим менеджером через форму зв’язку онлайн або ж телефонуйте','lionline');?>
 								<?php $button=get_field('phone',pll_current_language('slug'));  ?>
 								<?php if ($button ) : ?>
 									<a href="<?= $button['url'];?>"><?= $button['title'];?></a>
 								<?php endif; ?>
 							</div>
 							<div class="_404__btn"><a class="btn btn_brown" href="<?php echo esc_url( home_url( '/' ) ); ?>"><span><?php _e('на головну','lionline');?></span></a></div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</section>