<?php
/**
 * Template Name: Contacts Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
 <div class="contactPage">
  <div class="contactPage__image">
    <div class="contactPage__image-bg"></div><img src="<?php echo get_template_directory_uri();?>/dist/images/foto.png" alt="">
    <div class="contactPage__title"><span><?php the_title();?></span></div>
  </div>
  <div class="contact-map">
    <div class="row">
      <div class="map-info clearfix">
        <div class="map-info__title"><span><?php _e('Ми знаходимось за адресою','lionline');?></span></div>
        <div class="map-info__adress">
          <p><?php the_field('address');?></p>
        </div>
        <div class="map-info__contacts">
          <?php the_field('content');?>
        </div>
        <?php $button=get_field('phone',pll_current_language('slug'));  ?>
        <div class="map-info__btn"><a class="btn btn_brown"  data-open="openform"><span><?php _e('Забронювати','lionline');?></span></a>
          <div class="map-info__soc">
            <a href="<?php the_field('facebook_link',pll_current_language('slug'));  ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/fb_brown.svg" alt=""></a>
            <a href="<?php the_field('instagram_link',pll_current_language('slug'));  ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/inst_brown.svg" alt=""></a>
          </div>
        </div>
      </div>
    </div>
    <div id="map"></div>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApj8qSzyO4DqXeQtFQPKI4mi6p-MBwlpo&amp;callback=initMap"></script>
  </div>

</div>
<?php get_template_part( 'templates/block','reviews' );?>


<script>


  ////////////  map init /////////
  <?php $coordinates= get_field('map'); ?>
  <?php $button=get_field('phone',pll_current_language('slug'));  ?>
  <?php $route_link=get_field('marker_link');  ?>


  <?php $marker_informer="<div class='map-window'><img src='".get_template_directory_uri()."/dist/images/logo.svg'><a target='_blank' href='".$route_link['url']."'><span>GIRSKI hotel&spa </span><br>".$route_link['title']."</a><br><a href='".$button['url']."'>".$button['title']."</a> </div>"; ?>
  function initMap(){
    var element = document.getElementById('map');

    var options = {
      zoom: 17,
      center:{
        lat:<?= $coordinates['lat'];?>,
        lng:<?= $coordinates['lng'];?>

      }
    };

    var myMap = new google.maps.Map(element, options);


    var markers = [

    {
      coordinates:{
        lat:<?= $coordinates['lat'];?>,
        lng:<?= $coordinates['lng'];?>
      },
      // image:"assets/images/map_logo.png",
      info:"<?= $marker_informer;?>"
    }

    ];



    function addMarker(properties){
      var marker = new google.maps.Marker({
        position: properties.coordinates,
        map: myMap,
        icon:properties.image
      });

      if(properties.image){
        marker.setIcon(properties.image);
      }
      if(properties.info){
        marker.addListener('click',function(){
          InfoWindow.open(myMap, marker);
        });
        var InfoWindow = new google.maps.InfoWindow({
          content:properties.info
        });
      }
    }
    for(var i = 0; i < markers.length; i++){
      addMarker(markers[i]);
    }
  }



</script>

<?php endwhile; ?>
