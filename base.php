<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<?php get_template_part('templates/head'); ?>
<body>
  <?php get_template_part('templates/header'); ?>

  <?php include Wrapper\template_path(); ?>

  <?php wp_footer();?>
  <?php get_template_part( 'templates/form', 'booking' );?>


  <?php get_template_part( 'templates/footer','reviews' );?>

</body>
</html>