<?php
/**
 * Template Name: Room Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

	<section class="roomSlider-wrap">
    <slider>
      <div class="roomSlider">

        <?php
        $images = get_field('gallery');
        $size = 'full';
        if( $images ): ?>
        <?php foreach( $images as $image ): ?>
         <article>
          <div class="roomSlider-item">
            <div class="roomSlider-item__img">
              <div class="slide-bg"></div><?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
            </div>
          </div>
        </article>
      <?php endforeach; ?>
    <?php endif; ?>
  </div>
  <div class="roomSlider__title"><span class="btn"><?php the_title();?></span></div>
</slider>
</section>
<section class="room-price-wrap">
  <div class="row">
    <div class="room-price clearfix">
      <div class="room-price__title">
        <div class="title"><span><?php _e('ВАРТІСТЬ ПРОЖИВАННЯ','lionline');?></span></div>
      </div>
      <div class="room-price__price column large-6">
        <table class="minimalistBlack">
          <thead>
            <tr>
              <th><?php _e('Період','lionline');?></th>
              <th><?php _e('Ціна, грн','lionline');?></th>
            </tr>
          </thead>
          <tbody>
           <?php if(get_field('prices')): ?>
            <?php while(has_sub_field('prices')): ?>
              <tr>
                <td><?php the_sub_field('startdate'); ?> – <?php the_sub_field('enddate'); ?></td>
                <td><?php the_sub_field('price'); ?></td>
              </tr>
            <?php endwhile; ?>
          <?php endif; ?>
        </tbody>
      </table>
      <div class="price-text">
        <p><?php the_field('additional_text');?></p>
      </div>
      <?php $button=get_field('phone',pll_current_language('slug'));  ?>
      <div class="room-price__btn"><a class="btn btn_brown"  data-open="openform"><span><?php _e('Забронювати','lionline');?></span></a></div>
    </div>
    <div class="room-price__text column large-6">
      <?php the_field('description');?>
    </div>
  </div>
</div>
</section>
<section>
  <div class="row">
    <div class="inclusive clearfix">
      <div class="inclusive__title">
        <div class="title right"><span><?php _e('ВКЛЮЧЕНО','lionline');?></span></div>
      </div>
      <div class="inclusive__foto column large-6 medium-12"><img src="<?php the_field('image1');?>" alt=""><img src="<?php the_field('image2');?>" alt=""></div>
      <div class="inclusive__icons column large-6 medium-12">

        <?php if(get_field('advantages')): ?>
          <?php while(has_sub_field('advantages')): ?>
            <article class="inclusive-icon column large-4 medium-4 small-6">
              <div class="inclusive-icon__img"><img src="<?php the_sub_field('icon'); ?>" alt=""></div>
              <div class="inclusive-icon__text"><span><?php the_sub_field('title'); ?></span></div>
            </article>
          <?php endwhile; ?>
        <?php endif; ?>

      </div>
    </div>
  </div>
</section>
<script>
 jQuery(document).ready(function(){
  jQuery(".roomSlider").slick({
    slidesToShow: 1,
    arrows:true,
    autoplaySpeed: 6000,
    speed:1000,
    autoplay:true,
    dots:true,
    prevArrow: '<img class="room-arrow slide-prev" src="<?php echo get_template_directory_uri();?>/dist/images/slide-prev.svg" alt="">',
    nextArrow: '<img class="room-arrow slide-next" src="<?php echo get_template_directory_uri();?>/dist/images/slide-next.svg" alt="">',
    // fade:true,
    dotsClass: 'slick-control',
    responsive: [

    {
      breakpoint: 640,
      settings: {
       arrows:false,
       slidesToShow: 1
     }
   }
   ]
 });
});

</script>

<?php get_template_part( 'templates/block','restaurant' );?>
<?php get_template_part( 'templates/block','spa' );?>
<?php get_template_part( 'templates/block','conveniences' );?>
<?php get_template_part( 'templates/block','advantages' );?>

<?php endwhile; ?>
